#!/usr/bin/env python

import string

PARENS = ('(', ')')
OPERATORS = ('/', '*', '-', '+')

def arithmetic_eval(expression):
    print(list(lexer(expression)))
    print(list(infix_to_postfix(lexer(expression))))
    print(evaluate_infix(infix_to_postfix(lexer(expression))))

def lexer(expression):
    operand = ''
    for c in expression:

        if c in string.digits:
            operand = operand + c
            continue

        if operand:
            yield operand
            operand = ''

        if c in OPERATORS or c in PARENS:
            yield c

    if operand:
        yield operand

def infix_to_postfix(tokens):
    stack = []
    parens_stack = []
    for token in tokens:
        if token == '(':
            parens_stack.append(stack)
            stack = []
        elif token == ')':
            for item in flush_stack(stack):
                yield item
            stack = parens_stack.pop()
        elif token in OPERATORS:
            if len(stack) == 0:
                stack.append(token)
            else:
                top_precedence = OPERATORS.index(stack[-1])
                token_precedence = OPERATORS.index(token)
                while (len(stack) > 0 and top_precedence <= token_precedence):
                    yield stack.pop()
                    if len(stack) > 0:
                        top_precedence = OPERATORS.index(stack[-1])
                stack.append(token)
        else:
            #print('yield ' + str(token))
            yield token
        #print(stack, parens_stack)
    for token in flush_stack(stack):
        yield token

def flush_stack(stack):
    while len(stack) > 0:
        yield stack.pop()

def evaluate_infix(tokens):
    stack = []
    for token in tokens:
        if token == '+':
            stack.append(stack.pop() + stack.pop())
        elif token == '-':
            stack.append((-1) * stack.pop() + stack.pop())
        elif token == '*':
            stack.append(stack.pop() * stack.pop())
        elif token == '/':
            stack.append((1 / stack.pop()) * stack.pop())
        else:
            stack.append(int(token))
        if False:
            if token in OPERATORS:
                print(str(stack) + ' ' + token)
            else:
                print(stack)
    return stack[0]
            
if __name__ == '__main__':
    #expr = '2 + 3 * ( 1 - 4 * 2 )'
    #expr = '((2 * (0) - 2 * (3) * 1 - 1 * 1 - (2 + 3) * (2 - 4)))'
    expr = '1 - 1 - 1 - 1'
    #((0 - 6 - 1 - (-10)))'
    arithmetic_eval(expr)
