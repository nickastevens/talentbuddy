Arithmetic Evaluation
=====================

The most basic operations in computer programs are arithmetic operations like
this one:

    2 + 3 * (1 - 4 * 2)

Let’s find out how compilers understand the way programmers write arithmetic
expressions. Given an expression with integer values and the following
operators: + - * ( ) Your task is to write a function that parses this
expression, computes its result and prints it to standard output (stdout)

Note that your function will receive the following arguments:
    expression
    which is a string giving the expression that needs to be evaluated

Data constraints
================

* the expression will not have more than 10,000 characters and it is guaranteed
  to have no syntactic or semantic errors
* the integer constants in the expression range between [0, 1000]
* by following the standard evaluation ordering of the expression, no
  intermediary value, nor the final result may fall outside the range (-100000,
  100000)


Efficiency constraints
======================

* your function is expected to print the requested result and return in less
  than 2 seconds
